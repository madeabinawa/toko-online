import React, { Component } from 'react'
import Navigation from '../Navbar/Navigation';

export default class Catalog extends Component {
    constructor() {
        super()
        this.state = {
            length: 0,
            products: []
        }
        this.handleAdd = this.handleAdd.bind(this);
    }
    componentDidMount() {
        fetch('https://fakestoreapi.com/products?limit=10')
            .then(res => res.json())
            .then(res => this.setState({
                length: res.length,
                products: res
            }, console.log(res)))

    }
    handleAdd() {
        alert("Berhasil tambah ke Cart")
    }

    render() {
        const { products } = this.state
        const productsItem = products.map((product) =>

            <div key={product.id} className="col-md-3 mt-2 mb-2 d-flex align-items-stretch">
                <div className="card" style={{ width: "18rem" }}>
                    <img className="card-img-top" src={product.image} alt={product.title} height="100px" width="auto" />
                    <div className="card-body">
                        <h5 className="card-title text-dark">{product.title}</h5>
                        <p className="card-text text-dark">Rp.{product.price * 15000}</p>
                        <button className="btn btn-primary" onClick={() => this.handleAdd()}>Add to Cart</button>
                    </div>
                </div>
            </div>
        )

        return (
            <React.Fragment>
                <Navigation />

                <div className="jumbotron jumbotron-fluid">
                    <div className="container">
                        <div className="container">
                            <h1 className="display-4">Product Catalog</h1>
                        </div>
                    </div>

                </div>
                <div className="container">
                    <div className="row" >
                        {productsItem}
                    </div>
                </div>

            </React.Fragment>
        )
    }
}