import React from 'react'
import Navigation from '../components/Navbar/Navigation'

export default function About() {
    return (
        <div>
            <Navigation />
            <div className="jumbotron jumbotron-fluid">
                <div className="container">
                    <h1 className="display-4">Toko Online</h1>
                    <p className="lead">Toko Online menggunakan api dari Fakestoreapi</p>
                </div>
            </div>
        </div>
    )
}

