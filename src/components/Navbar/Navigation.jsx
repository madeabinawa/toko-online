import React, { Component } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Container from 'react-bootstrap/Container'
import { Link } from 'react-router-dom'

export default class Navigation extends Component {
    render() {
        return (

            <Navbar bg="dark" variant="dark" >
                <Container>
                    <Navbar.Brand to="/">Toko Online</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link as={Link} to="/catalog">Product Catalog</Nav.Link>
                        <Nav.Link as={Link} to="/about">About</Nav.Link>
                    </Nav>
                </Container>
            </Navbar >
        )
    }
}
