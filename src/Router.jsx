import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import Catalog from './components/Catalog/Catalog'
import About from './pages/About'
import Login from './pages/Login'

export default function Router() {
    return (
        <React.Fragment>
            <BrowserRouter>
                <Route exact path="/" component={Login} />
                <Route exact path="/catalog" component={Catalog} />
                <Route exact path="/about" component={About} />
            </BrowserRouter>
        </React.Fragment>
    )
}