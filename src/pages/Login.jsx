import React from 'react'
import logo from '../logo.svg';
import { Redirect } from 'react-router-dom'

export default class Login extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            redirect: null,
            email: '',
            password: '',
        }
    }

    // Menyimpan email yang diinput user kedalam state
    handleEmail = (event) => {
        this.setState({
            email: event.target.value
        })
    }
    // Menyimpan password yang diinput user kedalam state
    handlePassword = (event) => {
        this.setState({
            password: event.target.value
        })
    }

    handlerOnClick = () => {
        var isVerified = this.loginVerify(this.state.email, this.state.password)
        isVerified ? this.redirectToCatalog() : this.loginFailed()
    }

    // Melakukan verifikasi login
    loginVerify(username, password) {
        // inisialisasi username pasword
        const dataUsername = "madeabinawa@mail.com"
        const dataPassword = "12345"

        // verifikasi input password
        if (username === dataUsername && password === dataPassword) {
            return true
        }
    }

    redirectToCatalog() {
        this.setState({
            redirect: "/catalog"
        })
    }

    loginFailed() {
        alert("Email dan Password tidak ditemukan")
    }

    render() {
        return (
            <React.Fragment>
                <Redirect to={this.state.redirect} />
                <div className="container text-center mt-4">
                    <div className="row align-items-center">
                        <div className="col-md-6 offset-md-3">
                            <img className="mb-4" src={logo} alt="" width="100" />
                            <h1 className="h2 mb-3 font-weight-normal">Toko Online</h1>
                            <h1 className="h4 mb-3 font-weight-normal">Please Sign In</h1>

                            <label htmlFor="inputEmail" className="sr-only">Email address</label>
                            <input className="form-control m-1" type="email" onChange={this.handleEmail} id="inputEmail" placeholder="Email address" required />

                            <label htmlFor="inputPassword" className="sr-only">Password</label>
                            <input className="form-control m-1" type="password" onChange={this.handlePassword} id="inputPassword" placeholder="Password" required />

                            <button className="btn btn-lg btn-primary btn-block m-1" onClick={this.handlerOnClick}>Sign in</button>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}